<?php
function abc_get_intime_areas_list(){
	if( isset( $_POST['country_id'] ) ){
		$country_id = sanitize_text_field( $_POST['country_id'] );
		$lang = $_COOKIE['pll_language'];
		$lang = ( $lang == 'uk' ) ? 'ua' : $lang;
		$areas = as_intime_get_areas( $country_id );
		echo '<option value="">...</option>';
		foreach ( $areas as $area ) : ?>
			<option value="<?= $area->area_code; ?>"><?= $area->{ 'area_name_'.$lang } ?></option>
		<?php endforeach;
	}
	wp_die();
}
add_action( 'wp_ajax_as_intime_areas_list', 'abc_get_intime_areas_list' );
add_action( 'wp_ajax_nopriv_as_intime_areas_list', 'abc_get_intime_areas_list' );

function abc_get_intime_districts_list(){
	if( isset( $_POST['country_id'] ) && isset( $_POST['area_id'] ) ){
		$area_id = sanitize_text_field( $_POST['area_id'] );
		$country_id = sanitize_text_field( $_POST['country_id'] );
		$lang = $_COOKIE['pll_language'];
		$lang = ( $lang == 'uk' ) ? 'ua' : $lang;
		$districts = as_intime_get_districts( $area_id, $country_id );
		echo '<option value="">...</option>';
		foreach ( $districts as $district ) : ?>
			<option value="<?= $district->district_code; ?>"><?= $district->{ 'district_name_'.$lang } ?></option>
		<?php endforeach;
	}
	wp_die();
}
add_action( 'wp_ajax_as_intime_districts_list', 'abc_get_intime_districts_list' );
add_action( 'wp_ajax_nopriv_as_intime_districts_list', 'abc_get_intime_districts_list' );

function abc_get_intime_localities_list(){
	if( isset( $_POST['country_id'] )&& isset( $_POST['area_id'] ) && isset( $_POST['district_id'] ) ){
		$area_id = sanitize_text_field( $_POST['area_id'] );
		$country_id = sanitize_text_field( $_POST['country_id'] );
		$district_id = sanitize_text_field( $_POST['district_id'] );
		$lang = $_COOKIE['pll_language'];
		$lang = ( $lang == 'uk' ) ? 'ua' : $lang;
		$localities = as_intime_get_localities( $area_id, $district_id, $country_id );
		echo '<option value="">...</option>';
		foreach ( $localities as $locality ) : ?>
			<option value="<?= $locality->locality_code; ?>"><?= $locality->{ 'locality_name_'.$lang } ?></option>
		<?php endforeach;
	}
	wp_die();
}
add_action( 'wp_ajax_as_intime_localities_list', 'abc_get_intime_localities_list' );
add_action( 'wp_ajax_nopriv_as_intime_localities_list', 'abc_get_intime_localities_list' );

function abc_get_intime_branches_list(){
	if( isset( $_POST['country_id'] )&& isset( $_POST['area_id'] ) && isset( $_POST['district_id'] ) && isset( $_POST['locality_id'] ) ){
		$area_id = sanitize_text_field( $_POST['area_id'] );
		$country_id = sanitize_text_field( $_POST['country_id'] );
		$district_id = sanitize_text_field( $_POST['district_id'] );
		$locality_id = sanitize_text_field( $_POST['locality_id'] );
		$lang = $_COOKIE['pll_language'];
		$lang = ( $lang == 'uk' ) ? 'ua' : $lang;
		$branches = as_intime_get_branches( $country_id, $area_id, $district_id, $locality_id );
		echo '<option value="">...</option>';
		foreach ( $branches as $branch ) : ?>
			<option value="<?= $branch->branch_number.' - '.$branch->address_ru; ?>">№<?= $branch->branch_number.' - '.$branch->{ 'address_'.$lang } ?></option>
		<?php endforeach;
	}
	wp_die();
}
add_action( 'wp_ajax_as_intime_branches_list', 'abc_get_intime_branches_list' );
add_action( 'wp_ajax_nopriv_as_intime_branches_list', 'abc_get_intime_branches_list' );