<?php
add_action( 'init', 'as_add_pll_strings' );
function as_add_pll_strings(){
	// checkout area select input
	pll_register_string( 'CHECKOUT_AREA_LABEL', 'CHECKOUT_AREA_LABEL', 'InTime plugin' );
	pll_register_string( 'CHECKOUT_AREA_PLACEHOLDER', 'CHECKOUT_AREA_PLACEHOLDER', 'InTime plugin' );

	// checkout district input
	pll_register_string( 'CHECKOUT_DISTRICT_LABEL', 'CHECKOUT_DISTRICT_LABEL', 'InTime plugin' );
	pll_register_string( 'CHECKOUT_DISTRICT_PLACEHOLDER', 'CHECKOUT_DISTRICT_PLACEHOLDER', 'InTime plugin' );

	// checkout locality input
	pll_register_string( 'CHECKOUT_LOCALITY_LABEL', 'CHECKOUT_LOCALITY_LABEL', 'InTime plugin' );
	pll_register_string( 'CHECKOUT_LOCALITY_PLACEHOLDER', 'CHECKOUT_LOCALITY_PLACEHOLDER', 'InTime plugin' );

	// checkout branch input
	pll_register_string( 'CHECKOUT_BRANCH_LABEL', 'CHECKOUT_BRANCH_LABEL', 'InTime plugin' );
	pll_register_string( 'CHECKOUT_BRANCH_PLACEHOLDER', 'CHECKOUT_BRANCH_PLACEHOLDER', 'InTime plugin' );
}