<?php
function as_intime_get_countries(){
	require_once( AS_INTIME_DIR.'/classes/soap/get_country_by_id.php' );
	$struct = new get_country_by_id( INTIME_API_KEY );

	try{
		$soap_client = new SoapClient( INTIME_API_URL );
		$soapStruct = new SoapVar( $struct, SOAP_ENC_OBJECT, 'get_country_by_id' );
		return $soap_client->get_country_by_id( new SoapParam( $soapStruct, 'get_country_by_id' ) )->Entry_get_country_by_id;
	}
	catch( Exception $e ){
		return [];
		// echo $e->getMessage();
	}
}

function as_intime_get_areas( $country_id ){
	require_once( AS_INTIME_DIR.'/classes/soap/get_area_filtered.php' );
	// $country_id = '000000215';
	$struct = new get_area_filtered( INTIME_API_KEY, '', $country_id );

	try{
		$soap_client = new SoapClient( INTIME_API_URL );
		$soapStruct = new SoapVar( $struct, SOAP_ENC_OBJECT, 'get_area_filtered' );
		return $soap_client->get_area_filtered( new SoapParam( $soapStruct, 'get_area_filtered' ) )->Entry_get_area_filtered;
	}
	catch( Exception $e ){
		return [];
		// echo $e->getMessage();
	}
}

function as_intime_get_districts( $area_id, $country_id ){
	require_once( AS_INTIME_DIR.'/classes/soap/get_district_filtered.php' );
	// $area_id = '000000007';
	// $country_id = '000000215';
	$struct = new get_district_filtered( INTIME_API_KEY, '', $area_id, $country_id );

	try{
		$soap_client = new SoapClient( INTIME_API_URL );
		$soapStruct = new SoapVar( $struct, SOAP_ENC_OBJECT, 'get_district_filtered' );
		return $soap_client->get_district_filtered( new SoapParam( $soapStruct, 'get_district_filtered' ) )->Entry_get_district_filtered;
	}
	catch( Exception $e ){
		return [];
		// echo $e->getMessage();
	}
}

function as_intime_get_localities( $area_id, $district_id, $country_id ){
	require_once( AS_INTIME_DIR.'/classes/soap/get_locality_filtered.php' );
	// $area_id = '000000007';
	// $district_id = '000000127';
	// $country_id = '000000215';
	$struct = new get_locality_filtered( INTIME_API_KEY, '', $area_id, $district_id, $country_id );

	try{
		$soap_client = new SoapClient( INTIME_API_URL );
		$soapStruct = new SoapVar( $struct, SOAP_ENC_OBJECT, 'get_locality_filtered' );
		return $soap_client->get_locality_filtered( new SoapParam( $soapStruct, 'get_locality_filtered' ) )->Entry_get_locality_filtered;
	}
	catch( Exception $e ){
		return [];
		// echo $e->getMessage();
	}
}

function as_intime_get_branches( $country_id, $area_id, $district_id, $locality_id ){
	require_once( AS_INTIME_DIR.'/classes/soap/get_branch_filtered.php' );
	// $country_id = '000000215';
	// $area_id = '000000007';
	// $district_id = '000000130';
	// $locality_id = '000000089';
	$struct = new get_branch_filtered( INTIME_API_KEY, '', $country_id, $area_id, $district_id, $locality_id );

	try{
		$soap_client = new SoapClient( INTIME_API_URL );
		$soapStruct = new SoapVar( $struct, SOAP_ENC_OBJECT, 'get_branch_filtered' );
		return $soap_client->get_branch_filtered( new SoapParam( $soapStruct, 'get_branch_filtered' ) )->Entry_get_branch_filtered;
	}
	catch( Exception $e ){
		return [];
		// echo $e->getMessage();
	}
}