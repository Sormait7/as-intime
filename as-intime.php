<?php
/*
 * Plugin Name: Woo Intime
 * Plugin URI: http://www.actualsolutions.net/
 * Description: Interface for using intime delivery service and get from client region, city, warehouse
 * Version: 1.0
 * Author: Paul Strelkovsky
 */
ini_set('display_errors', 1);
error_reporting( E_ALL );

// direct access protection
defined( 'ABSPATH' ) or die();

// api url
define( 'INTIME_API_URL', 'http://esb.intime.ua:8080/services/intime_api_3.0?wsdl' );

// api key
$intime_options = get_option( 'woocommerce_intime_settings' );
if( is_array( $intime_options ) && array_key_exists( 'api_key', $intime_options ) ){
	define( 'INTIME_API_KEY', $intime_options['api_key'] );
}
else{
	define( 'INTIME_API_KEY', '' );
}

define( 'AS_INTIME_DIR', dirname( __FILE__ ) );

// functions for simplifying soap requets to the intime api service
require_once( 'inc/api_helpers.php' );

// handlers for ajax requests
require_once( 'inc/ajax_handlers.php' );

require_once( 'inc/pll_strings.php' );

// including and registering shipping method class
function as_register_intime_shipping() {
    require_once( 'classes/as-intime-shipping.php' );
}
add_action( 'woocommerce_shipping_init', 'as_register_intime_shipping' );

// adding shipping method to already existed
function as_add_intime_shipping( $methods ) {
    $methods['intime'] = 'AS_InTime_Shipping'; 
    return $methods;
}
add_filter( 'woocommerce_shipping_methods', 'as_add_intime_shipping' );

// change default checkout fields
add_filter( 'woocommerce_checkout_fields' , 'as_intime_checkout_fields' );
function as_intime_checkout_fields( $fields ) {
	$chosen_methods = WC()->session->get('chosen_shipping_methods');
 	if (is_array($chosen_methods) && in_array('intime', $chosen_methods)) {

		$lang = pll_current_language();
		$lang = ( $lang == 'uk' ) ? 'ua' : $lang;

 		// areas default list
 		$areas_raw = as_intime_get_areas( '000000215' );
 		$areas = [];
 		$areas[] = '...';
 		foreach( $areas_raw as $area ){
 			$areas[ $area->area_code ] = $area->{ 'area_name_'.$lang };
 		}
 		
 		/* shipping */
 		unset( $fields['shipping']['shipping_country'] );
 		unset( $fields['shipping']['shipping_company'] );
 		unset( $fields['shipping']['shipping_address_1'] );
 		unset( $fields['shipping']['shipping_address_2'] );
 		unset( $fields['shipping']['shipping_postcode'] );
 		unset( $fields['shipping']['shipping_state'] );
 		unset( $fields['shipping']['shipping_city'] );

	 	$fields['shipping']['shipping_intime_area'] = array(
	 	    'label'     => pll__( 'CHECKOUT_AREA_LABEL' ),
		    'placeholder'   => pll__( 'CHECKOUT_AREA_PLACEHOLDER' ),
		    'required'  => false,
		    'class'     => array('form-row-wide'),
		    'clear'     => true,
		    'id'		=> 'as_intime_areas_input',
		    'type' 		=> 'select',
		    'options' 	=> $areas,
		);

		$fields['shipping']['shipping_intime_district'] = array(
	 	    'label'     => pll__( 'CHECKOUT_DISTRICT_LABEL' ),
		    'placeholder'   => pll__( 'CHECKOUT_DISTRICT_PLACEHOLDER' ),
		    'required'  => false,
		    'class'     => array('form-row-wide'),
		    'clear'     => true,
		    'id' 		=> 'as_intime_districts_input',
		    'type' 		=> 'select',
		    'options' 	=> [ 0 => '...' ],
		);

		$fields['shipping']['shipping_intime_locality'] = array(
	 	    'label'     => pll__( 'CHECKOUT_LOCALITY_LABEL' ),
		    'placeholder'   => pll__( 'CHECKOUT_LOCALITY_PLACEHOLDER' ),
		    'required'  => false,
		    'class'     => array('form-row-wide'),
		    'clear'     => true,
		    'id' 		=> 'as_intime_localities_input',
		    'type' 		=> 'select',
		    'options' 	=> [ 0 => '...' ],
		);

		$fields['shipping']['shipping_intime_warehouse'] = array(
	        'label'     => pll__( 'CHECKOUT_BRANCH_LABEL' ),
		    'placeholder'   => pll__( 'CHECKOUT_BRANCH_PLACEHOLDER' ),
		    'required'  => false,
		    'class'     => array('form-row-wide'),
		    'clear'     => true
		);

		/* billing */
		unset( $fields['billing']['billing_country'] );
		unset( $fields['billing']['billing_company'] );
 		unset( $fields['billing']['billing_address_1'] );
 		unset( $fields['billing']['billing_address_2'] );
 		unset( $fields['billing']['billing_postcode'] );
 		unset( $fields['billing']['billing_state'] );
 		unset( $fields['billing']['billing_city'] );
 		
	 	$fields['billing']['billing_intime_area'] = array(
	 	    'label'     => pll__( 'CHECKOUT_AREA_LABEL' ),
		    'placeholder'   => pll__( 'CHECKOUT_AREA_PLACEHOLDER' ),
		    'required'  => true,
		    'class'     => array('form-row-wide'),
		    'clear'     => true,
		    'id'		=> 'as_intime_areas_input',
		    'type' 		=> 'select',
		    'options' 	=> $areas,
		);

		$fields['billing']['billing_intime_district'] = array(
	 	    'label'     => pll__( 'CHECKOUT_DISTRICT_LABEL' ),
		    'placeholder'   => pll__( 'CHECKOUT_DISTRICT_PLACEHOLDER' ),
		    'required'  => true,
		    'class'     => array('form-row-wide'),
		    'clear'     => true,
		    'id' 		=> 'as_intime_districts_input',
		    'type' 		=> 'select',
		    'options' 	=> [ 0 => '...' ],
		);

		$fields['billing']['billing_intime_locality'] = array(
	 	    'label'     => pll__( 'CHECKOUT_LOCALITY_LABEL' ),
		    'placeholder'   => pll__( 'CHECKOUT_LOCALITY_PLACEHOLDER' ),
		    'required'  => true,
		    'class'     => array('form-row-wide'),
		    'clear'     => true,
		    'id' 		=> 'as_intime_localities_input',
		    'type' 		=> 'select',
		    'options' 	=> [ 0 => '...' ],
		);

		$fields['billing']['billing_intime_warehouse'] = array(
	        'label'     => pll__( 'CHECKOUT_BRANCH_LABEL' ),
		    'placeholder'   => pll__( 'CHECKOUT_BRANCH_PLACEHOLDER' ),
		    'required'  => true,
		    'class'     => array('form-row-wide'),
		    'clear'     => true,
		    'id' 		=> 'as_intime_branches_input',
		    'type' 		=> 'select',
		    'options' 	=> [ 0 => '...' ],
		);
 	}

    return $fields;
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'as_intime_checkout_field_display_admin_order_meta', 10, 1 );
function as_intime_checkout_field_display_admin_order_meta( $order ){
	if( ( 'ИнТайм' == $order->get_shipping_method() ) || ( 'Интайм' == $order->get_shipping_method() ) || ( 'Intime' == $order->get_shipping_method() ) ){
	    echo '<p><strong>'.'Способ доставки:</strong> ' . $order->get_shipping_method(). '</p>';
	    echo '<p><strong>'.'Адрес доставки: </strong> ' . get_post_meta( $order->get_id(), '_intime_delivery_address', true ) . '</p>';
	}
}

add_filter( 'woocommerce_default_address_fields' , 'as_intime_address_fields' );
function as_intime_address_fields( $address_fields ) {
    $address_fields['address_1']['required'] = false;
    return $address_fields;
}

add_action( 'woocommerce_after_order_notes', 'as_intime_checkout_delivery_address_hidden_field' );
function as_intime_checkout_delivery_address_hidden_field( $checkout ) {

    // Output the hidden field
    echo '<input type="hidden" class="input-hidden" name="intime_delivery_address" id="as_intime_delivery_address_input" value="">';
}

// Saving the hidden field value in the order metadata
add_action( 'woocommerce_checkout_update_order_meta', 'save_intime_delivery_address_checkout_hidden_field' );
function save_intime_delivery_address_checkout_hidden_field( $order_id ) {
    if ( ! empty( $_POST['intime_delivery_address'] ) ) {
        update_post_meta( $order_id, '_intime_delivery_address', sanitize_text_field( $_POST['intime_delivery_address'] ) );
    }
}

add_action( 'woocommerce_after_checkout_form', 'as_intime_add_checkout_script' );
function as_intime_add_checkout_script(){
	?>
	<script type="text/javascript">
		( function( $ ){
			$( document ).ready( function(){
				window.shipping_method_changed = 0;
				$( document ).on( 'click', '#shipping_method input[type=radio]', function(){
					window.shipping_method_changed = 1;
				} );

				$( document ).ajaxComplete(function( event, xhr, settings ) {
					if( window.shipping_method_changed && ( settings.url == '/?wc-ajax=update_order_review' ) ){
						location.reload();
						console.log( settings );
					}
				});

			} );
		} )( jQuery );
	</script>
	<?php
}

add_action( 'wp_footer', function(){ ?>
	<script type="text/javascript">
		( function( $ ){
			$( document ).ready( function(){
				// country change
				$( document ).on( 'change', '#as_intime_countries_input', function(){
					var country_id = $( this ).val();
					$.ajax( {
						url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						type: 'POST',
						data: {
							action: 'as_intime_areas_list',
							country_id: country_id,
						}
					} ).done( function( response ){
						$( '#as_intime_areas_input' ).html( response );
						$( '#as_intime_districts_input' ).html( '<option value="">...</option>' );
						$( '#as_intime_localities_input' ).html( '<option value="">...</option>' );
						$( '#as_intime_branches_input' ).html( '<option value="">...</option>' );
					} );
				} );

				// area change
				$( document ).on( 'change', '#as_intime_areas_input', function(){
					// var country_id = $( '#as_intime_countries_input' ).val();
					var country_id = '000000215';
					var area_id = $( this ).val();
					$.ajax( {
						url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						type: 'POST',
						data: {
							action: 'as_intime_districts_list',
							country_id: country_id,
							area_id: area_id,
						}
					} ).done( function( response ){
						$( '#as_intime_districts_input' ).html( response );
						$( '#as_intime_localities_input' ).html( '<option value="">...</option>');
						$( '#as_intime_branches_input' ).html( '<option value="">...</option>' );
					} );
				} );

				// district change
				$( document ).on( 'change', '#as_intime_districts_input', function(){
					// var country_id = $( '#as_intime_countries_input' ).val();
					var country_id = '000000215';
					var area_id = $( '#as_intime_areas_input' ).val();
					var district_id = $( this ).val();
					$.ajax( {
						url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						type: 'POST',
						data: {
							action: 'as_intime_localities_list',
							country_id: country_id,
							area_id: area_id,
							district_id: district_id,
						}
					} ).done( function( response ){
						$( '#as_intime_localities_input' ).html( response );
						$( '#as_intime_branches_input' ).html( '<option value="">...</option>' );
					} );
				} );

				// locality change
				$( document ).on( 'change', '#as_intime_localities_input', function(){
					// var country_id = $( '#as_intime_countries_input' ).val();
					var country_id = '000000215';
					var area_id = $( '#as_intime_areas_input' ).val();
					var district_id = $( '#as_intime_districts_input' ).val();
					var locality_id = $( this ).val();
					$.ajax( {
						url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						type: 'POST',
						data: {
							action: 'as_intime_branches_list',
							country_id: country_id,
							area_id: area_id,
							district_id: district_id,
							locality_id: locality_id,
						}
					} ).done( function( response ){
						$( '#as_intime_branches_input' ).html( response );
						update_intime_delivery_address();
					} );
				} );

				// branch change
				$( document ).on( 'change', '#as_intime_branches_input', function(){
					update_intime_delivery_address();
				} );

				function update_intime_delivery_address(){
					var a = $( '#as_intime_areas_input' ).children( ':selected' ).text();
					var b = $( '#as_intime_districts_input' ).children( ':selected' ).text();
					var c = $( '#as_intime_localities_input' ).children( ':selected' ).text();
					var d = $( '#as_intime_branches_input' ).children( ':selected' ).text();
					var v = a + '; ' + b + '; ' + c + '; ' + d;
					console.log( v );
					$( '#as_intime_delivery_address_input' ).val( v );
				}
			} );
		} )( jQuery );
	</script>
<?php } );