<?php
class get_area_filtered{
	public $api_key;
	public $id;
	public $country_id;
	public $area_name; 

	public function __construct( $key = '', $id = '', $country_id = '', $area_name = '' ){
		$this->api_key = $key;
		$this->id = $id;
		$this->country_id = $country_id;
		$this->area_name = $area_name;
	}
}