<?php
class get_district_filtered{
	public $api_key;
	public $id;
	public $area_id; 
	public $country_id;
	public $district_name;

	public function __construct( $key = '', $id = '', $area_id = '', $country_id = '', $district_name = '' ){
		$this->api_key = $key;
		$this->id = $id;
		$this->area_id = $area_id;
		$this->country_id = $country_id;
		$this->district_name = $district_name;
	}
}