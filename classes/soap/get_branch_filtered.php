<?php
class get_branch_filtered{
	public $api_key;
	public $id;
	public $country_id;
	public $area_id; 
	public $district_id;
	public $locality_id;
	public $branch_name;

	public function __construct( $key = '', $id = '', $country_id = '', $area_id = '', $district_id = '', $locality_id = '', $branch_name = '' ){
		$this->api_key = $key;
		$this->id = $id;
		$this->country_id = $country_id;
		$this->area_id = $area_id;
		$this->district_id = $district_id;
		$this->locality_id = $locality_id;
		$this->branch_name = $branch_name;
	}
}