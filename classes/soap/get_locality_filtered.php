<?php
class get_locality_filtered{
	public $api_key;
	public $id;
	public $area_id; 
	public $district_id;
	public $country_id;
	public $locality_name;

	public function __construct( $key = '', $id = '', $area_id = '', $district_id = '', $country_id = '', $locality_name = '' ){
		$this->api_key = $key;
		$this->id = $id;
		$this->area_id = $area_id;
		$this->district_id = $district_id;
		$this->country_id = $country_id;
		$this->locality_name = $locality_name;
	}
}