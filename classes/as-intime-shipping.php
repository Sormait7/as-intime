<?php
/**
 * class AS_InTime_Shipping
 * 
 * provides specifying region, city, warehouse in checkout data
 * and order details page
 * 
 * @package as-intime
 * @author Paul Strelkovsky
 * @version 1.0
 */

if ( ! class_exists( 'AS_InTime_Shipping' ) ) {
    class AS_InTime_Shipping extends WC_Shipping_Method {
        /**
         * Constructor for shipping class
         *
         * @access public
         * @return void
         */
        public function __construct() {
            $this->id                 = 'intime';
            $this->method_title       = __( 'ИнТайм' );
            $this->method_description = __( 'Доставка сервисом ИнТайм' );

            $this->availability = 'including';
            $this->countries = array(
                'UA',
            );

            $this->init();
			$this->enabled = $this->get_option( 'enabled' );            
			$this->title = $this->get_option( 'title' );
			$this->description = $this->get_option( 'decription' );
        }

        /**
         * Init settings
         *
         * @access public
         * @return void
         */
        function init() {
            // Load the settings API
            $this->init_form_fields(); 
            $this->init_settings(); 

            if ( is_admin() ) {
            	add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
            }
        }

        /**
         * calculate_shipping function.
         *
         * @access public
         * @param mixed $package
         * @return void
         */
        public function calculate_shipping( $package ){
            $rate = array(
				'id' => $this->id,
				'label' => $this->title,
				'cost' => '0',
				'calc_tax' => 'per_item'
			);
			$this->add_rate( $rate );
        }

        public function init_form_fields(){
		    $this->form_fields = array(
		    'enabled' => array(
		        'title' => __( 'Enabled', 'woocommerce' ),
		        'type' => 'checkbox',
		        'description' => 'Активировать метод доставки',
		        'default' => 'yes'
		        ),
		    'title' => array(
		        'title' => __( 'Title', 'woocommerce' ),
		        'type' => 'text',
		        'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
		        'default' => __( 'ИнТайм', 'woocommerce' )
		        ),
		    'description' => array(
		        'title' => __( 'Description', 'woocommerce' ),
		        'type' => 'textarea',
		        'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ),
		        ),
		    'api_key' => array(
		        'title' => 'Ключ API',
		        'type' => 'text',
		        'description' => 'Ключ, полученный в <a href="https://intime.ua/ua-api" target="_blank">личном кабинете</a> на сайте InTime',
		        )
		    );
		}
    }
}